<%@include file="../jspf/header.jspf"%>

<div class="navbar navbar-default navbar-fixed-top">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-responsive-collapse">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="./">Event EE</a>
	</div>
	<c:if test="${!annonyme}">
		<div class="navbar-collapse collapse navbar-responsive-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="./"> <span
						class="glyphicon glyphicon-arrow-left"></span></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="./detruiresession">Se deconnecter</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><p class="navbar-text">
						<c:out value="${authentification.participant.prenom}" />
						<c:out value="${authentification.participant.nom}" />
						|
					</p></li>
			</ul>
		</div>
	</c:if>
</div>

<div class="container">

	<div class="row">
		<div class="col-md-12">
			<div class="well bs-component">
				La page demand�e est introuvable
			</div>
		</div>
	</div>
</div>



<%@include file="../jspf/footer.jspf"%>