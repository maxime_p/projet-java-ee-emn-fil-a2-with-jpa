<%@include file="../jspf/header.jspf"%>

<div class="navbar navbar-default navbar-fixed-top">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-responsive-collapse">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="./">Event EE</a>
	</div>
	<div class="navbar-collapse collapse navbar-responsive-collapse">
		<ul class="nav navbar-nav">
			<li class="active"><a href="./"> <span class="glyphicon glyphicon-arrow-left"></span></a></li>
		</ul>
	</div>
</div>
<div class="container">
	<div class="bs-docs-section">
		<div class="row">
			<div class="col-lg-12">
				<div class="well bs-component">
					<form class="form-horizontal" method="post"
						action="<c:url value="/inscription"/>">
						<fieldset>
							<legend>
								<h2 class="form-signin-heading">Inscription</h2>
							</legend>
							<div class="form-group">
								<label for="inputEmail" class="col-lg-2 control-label">Email</label>
								<div class="col-lg-10">
									<input type="email" class="form-control" name="inputEmail"
										id="inputEmail" placeholder="Email">
								</div>
							</div>
							<div class="form-group">
								<label for="inputNom" class="col-lg-2 control-label">Nom</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="inputNom"
										id="inputNom" placeholder="Nom">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPrenom" class="col-lg-2 control-label">Pr�nom</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="inputPrenom"
										id="inputPrenom" placeholder="Pr�nom">
								</div>
							</div>
							<div class="form-group">
								<label for="inputSociete" class="col-lg-2 control-label">Soci�t�</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" name="inputSociete"
										id="inputSociete" placeholder="Soci�t�">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword" class="col-lg-2 control-label">Mot
									de passe</label>
								<div class="col-lg-10">
									<input type="password" class="form-control"
										name="inputPassword" id="inputPassword"
										placeholder="Mot de passe">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword2" class="col-lg-2 control-label">Confirmer
									le Mot de passe</label>
								<div class="col-lg-10">
									<input type="password" class="form-control"
										name="inputPassword2" id="inputPassword2"
										placeholder="Mot de passe">

									<div class="checkbox">
										<label> <input type="checkbox"> J'accepte les
											conditions d'utilisation
										</label>
									</div>
								</div>
							</div>
							<div class="alert alert-danger" role="alert"
								style="display:<c:out value='${message != null ? "" : "none"}'/>;">
								<c:out value="${message}" />
							</div>
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<button class="btn btn-default">Annuler</button>
									<button type="submit" class="btn btn-primary">Valider</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file="../jspf/footer.jspf"%>