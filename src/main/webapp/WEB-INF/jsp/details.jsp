<%@include file="../jspf/header.jspf"%>

<div class="navbar navbar-default navbar-fixed-top">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-responsive-collapse">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="./">Event EE</a>
	</div>
	<c:if test="${!annonyme}">
		<div class="navbar-collapse collapse navbar-responsive-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="./"> <span
						class="glyphicon glyphicon-arrow-left"></span></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="./detruiresession">Se deconnecter</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><p class="navbar-text">
						<c:out value="${authentification.participant.prenom}" />
						<c:out value="${authentification.participant.nom}" />
						|
					</p></li>
			</ul>
		</div>
	</c:if>
</div>

<div class="container">
	<c:if test="${inscriptionOK}">
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
			</button>
			<strong>Inscription effectu�e avec succ�s</strong>
		</div>
	</c:if>
	<c:if test="${inscriptionKO}">
		<div class="alert alert-warning alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
			</button>
			<strong>Probl�me lors de l'inscription</strong>
		</div>
	</c:if>
	<div class="row">
		<div class="col-md-4">
			<div class="well bs-component">
				<h2>
					<c:out value="${evenement.nom}" />
				</h2>
				<c:if test="${auteur}">
					<p class="bg-info">Vous �tes l'auteur</p>
				</c:if>

				<p>
					Lieu :
					<c:out value="${evenement.adresse}" />
				</p>
				<p>
					Date de d�but :
					<c:out value="${evenement.debut}" />
					<c:out value="${evenement.hdebut}" />
				</p>
				<p>
					Date de fin :
					<c:out value="${evenement.fin}" />
					<c:out value="${evenement.hfin}" />
				</p>
				<p>
					<c:if test="${!inscription}">
					Publi� : <span
							class="glyphicon glyphicon-<c:out value='${evenement.publie == 1 ? "ok-circle" : "remove-circle"}'/>"></span>
					</c:if>
				</p>
				<p>
					<c:if test="${annonyme}">
						<hr>
						<form class="form-horizontal" method="post"
							action="<c:url value="/inscriptionevenement?e=${evenement.id}"/>">
							<%@include file="../jspf/form.jspf"%>
						</form>
					</c:if>
					<c:if test="${!annonyme && !inscription}">
						<a href="./inscriptionevenement?e=${evenement.id}"
							class="btn btn-primary">S'inscrire</a>
					</c:if>
					<c:if test="${!annonyme && inscription}">
						<a href="#" class="btn btn-primary" disabled="disabled">D�j�
							inscrit</a>
					</c:if>
				</p>
			</div>
		</div>
		<div class="col-md-8">
			<div class="well bs-component">
				<h2>Participants :</h2>
				<p>
					<c:if test="${empty participe}">
						<p class="bg-info">Aucun participant</p>
					</c:if>
					<c:if test="${!empty participe}">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>email</th>
									<th>Nom</th>
									<th>Prenom</th>
									<th>Societe</th>
								</tr>
							</thead>
							<tbody class="list">
								<c:forEach items="${participe}" var="item" varStatus="loop">
									<tr>
										<td><c:out value='${item.email}' /></td>
										<td><c:out value='${item.nom}' /></td>
										<td><c:out value='${item.prenom}' /></td>
										<td><c:out value='${item.societe}' /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</c:if>

				</p>
			</div>
		</div>
	</div>
</div>


<%@include file="../jspf/footer.jspf"%>