<%@include file="../jspf/header.jspf"%>

<div class="navbar navbar-default navbar-fixed-top">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-responsive-collapse">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="javascript:void(0)">Event EE</a>
	</div>
	<div class="navbar-collapse collapse navbar-responsive-collapse">
		<ul class="nav navbar-nav">
			<li class="active"><a href="./creerevenement">Ajouter</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="./detruiresession">Se deconnecter</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><p class="navbar-text">
					<c:out value="${authentification.participant.prenom}" />
					<c:out value="${authentification.participant.nom}" />
					|
				</p></li>
		</ul>
	</div>
</div>

<div class="container">

	<div class="row">
		<div class="col-md-12">
			<div class="well bs-component">
				<div id="users">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li class="active"><a href="#cree" role="tab"
							data-toggle="tab">Cr��</a></li>
						<li><a href="#participe" role="tab" data-toggle="tab">Participe</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="cree">
							<div class="form-inline">
								<div class="form-group">
									<input class="search form-control col-lg-4"
										placeholder="Search">
								</div>
								<div class="form-group">
									<button class="sort btn btn-primary" data-sort="name">Trier
										par nom</button>
								</div>
							</div>
							<table class="table table-striped">
								<thead>
									<tr>
										<th>#</th>
										<th>Nom de l'�v�nement</th>
										<th>Publication</th>
										<th>Suppression</th>
									</tr>
								</thead>
								<tbody class="list">
									<c:forEach items="${evenements}" var="item" varStatus="loop">
										<tr>
											<td><h5><c:out value='${loop.index}' /></h5></td>
											<td><a
												href="./afficherinfo?e=<c:out value='${item.id}'/>"><h4 class="name">
														<c:out value='${item.nom}' />
													</h4></a></td>
											<td><a
												href="./publierevenement?e=<c:out value='${item.id}'/>"
												class="btn btn-primary"
												<c:out value='${item.publie == 1 ? "disabled" : ""}'/>>Publier</a></td>
											<td><a
												href="./supprimerevenement?e=<c:out value='${item.id}'/>"
												class="btn btn-danger">Supprimer</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div class="tab-pane" id="participe">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>#</th>
										<th>Nom de l'�v�nement</th>
										<th>Publication</th>
										<th>Suppression</th>
									</tr>
								</thead>
								<tbody class="list">
									<c:forEach items="${participe}" var="item" varStatus="loop">
										<tr>
											<td><c:out value='${loop.index}' /></td>
											<td class="name"><a
												href="./afficherinfo?e=<c:out value='${item.id}'/>"><h4>
														<c:out value='${item.nom}' />
													</h4></a></td>
											<td><a
												href="./supprimerevenement?e=<c:out value='${item.id}'/>"
												class="btn btn-danger">Se d�sinscrire</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<c:url value="/inc/js/list.js"/>"></script>
	<script>
		var options = {
			valueNames : [ 'name' ]
		};

		var userList = new List('users', options);
	</script>

	<%@include file="../jspf/footer.jspf"%>