<%@include file="../jspf/header.jspf"%>

<div class="navbar navbar-default navbar-fixed-top">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-responsive-collapse">
			<span class="icon-bar"></span> <span class="icon-bar"></span> <span
				class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="./">Event EE</a>
	</div>
</div>
<div class="container">
	<div class="bs-docs-section">
		<div class="row">
			<div class="col-lg-12">
				<div class="well bs-component">
					<form method="post" action="<c:url value="/connexion"/>"
						class="form-horizontal">
						<fieldset>
							<legend>
								<h2 class="form-signin-heading">connexion</h2>
							</legend>
							<div class="form-group">
								<label for="inputEmail" class="col-lg-2 control-label">Email</label>
								<div class="col-lg-10">
									<input type="email" class="form-control" id="inputEmail"
										placeholder="Email" name="inputEmail">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword" class="col-lg-2 control-label">Mot
									de passe</label>
								<div class="col-lg-10">
									<input type="password" class="form-control" id="inputPassword"
										placeholder="Mot de passe" name="inputPassword">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-10 col-lg-offset-2">
									<button type="submit" class="btn btn-primary">Se
										connecter</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
				<div class="alert alert-danger" role="alert"
					style="display:<c:out value='${message != null ? "" : "none"}'/>;">
					<c:out value="${message}" />
				</div>
			</div>
			<div class="col-lg-12">
				<div class="well bs-component">
					<p>
						Vous n'avez pas de compte : <a
							href="<c:url value="./inscription"/>">Inscrivez vous</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<%@include file="../jspf/footer.jspf"%>