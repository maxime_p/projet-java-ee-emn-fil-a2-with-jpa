/*
 * Created on 28 oct. 2014 ( Time 11:59:30 )
 * Generated by Telosys Tools Generator ( version 2.1.0 )
 */
package org.demo.persistence.services.fake;

import java.util.List;
import java.util.Map;

import org.demo.bean.jpa.ParticipeEntity;
import org.demo.persistence.commons.fake.GenericFakeService;
import org.demo.persistence.services.ParticipePersistence;

/**
 * Fake persistence service implementation ( entity "Participe" )
 *
 * @author Telosys Tools Generator
 */
public class ParticipePersistenceFAKE extends GenericFakeService<ParticipeEntity> implements ParticipePersistence {

	public ParticipePersistenceFAKE () {
		super(ParticipeEntity.class);
	}
	
	protected ParticipeEntity buildEntity(int index) {
		ParticipeEntity entity = new ParticipeEntity();
		// Init fields with mock values
		entity.setIdEv( nextInteger() ) ;
		entity.setIdUt( nextInteger() ) ;
		return entity ;
	}
	
	
	public boolean delete(ParticipeEntity entity) {
		log("delete ( ParticipeEntity : " + entity + ")" ) ;
		return true;
	}

	public boolean delete( Integer idEv, Integer idUt ) {
		log("delete ( PK )") ;
		return true;
	}

	public void insert(ParticipeEntity entity) {
		log("insert ( ParticipeEntity : " + entity + ")" ) ;
	}

	public ParticipeEntity load( Integer idEv, Integer idUt ) {
		log("load ( PK )") ;
		return buildEntity(1) ; 
	}

	public List<ParticipeEntity> loadAll() {
		log("loadAll()") ;
		return buildList(40) ;
	}

	public List<ParticipeEntity> loadByNamedQuery(String queryName) {
		log("loadByNamedQuery ( '" + queryName + "' )") ;
		return buildList(20) ;
	}

	public List<ParticipeEntity> loadByNamedQuery(String queryName, Map<String, Object> queryParameters) {
		log("loadByNamedQuery ( '" + queryName + "', parameters )") ;
		return buildList(10) ;
	}

	public ParticipeEntity save(ParticipeEntity entity) {
		log("insert ( ParticipeEntity : " + entity + ")" ) ;
		return entity;
	}

	public List<ParticipeEntity> search(Map<String, Object> criteria) {
		log("search (criteria)" ) ;
		return buildList(15) ;
	}

	@Override
	public long countAll() {
		return 0 ;
	}

}
