package services;

import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.demo.bean.jpa.EvenementEntity;
import org.demo.persistence.PersistenceServiceProvider;
import org.demo.persistence.services.EvenementPersistence;
import org.demo.persistence.services.jpa.EvenementPersistenceJPA;

import services.interfaces.EvenementService;
import exceptions.EvenementException;

public class EvenementServiceImpl implements EvenementService {

    @Override
    public void creerEvenement(EvenementEntity e) throws EvenementException {
        EvenementPersistence serviceE = PersistenceServiceProvider
                .getService(EvenementPersistence.class);
        serviceE.insert(e);

    }

    @Override
    public void supprimerEvenement(int id) {
        EvenementPersistenceJPA epj = new EvenementPersistenceJPA();
        HashMap<String, Object> creerMap = new HashMap<String, Object>();
        creerMap.put("id", id);
        if (epj.search(creerMap).size() == 1) {
            EvenementPersistence eventP = new EvenementPersistenceJPA();
            eventP.delete(id);
        }

    }

    @Override
    public void publierEvenement(int id) {
        EvenementPersistenceJPA epj = new EvenementPersistenceJPA();
        HashMap<String, Object> creerMap = new HashMap<String, Object>();
        creerMap.put("id", id);
        if (epj.search(creerMap).size() == 1) {
            System.out.println("in");
            EntityManagerFactory emf = Persistence
                    .createEntityManagerFactory("persistence-unit1");
            EntityManager em = emf.createEntityManager();
            EvenementEntity e = em.find(EvenementEntity.class, id);
            em.getTransaction().begin();
            e.setPublie(1);
            em.getTransaction().commit();
            try {
                em.flush();
            } catch (Exception ee) {
                System.out.println("Transaction required");
            }
        }

    }

}
