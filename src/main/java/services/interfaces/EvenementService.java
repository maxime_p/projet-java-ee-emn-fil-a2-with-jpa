package services.interfaces;

import org.demo.bean.jpa.EvenementEntity;

import exceptions.EvenementException;

public interface EvenementService {
	
	/**
	 * Ajoute l'evenement en base de données
	 * @param e evenement que l'on souhaite sauvegarder
	 * @throws EvenementException 
	 */
	public void creerEvenement(EvenementEntity e) throws EvenementException;
	
	/**
	 * Supprime l'evenement dont l'id est passé en parametre
	 * @param id de l'evenement
	 */
	public void supprimerEvenement(int id);
	
	/**
	 * Passe un evenement à l'état publié
	 * Si l'evenement est déjà publié, aucune modification n'est apportée
	 * @param id de l'evenement
	 */
	public void publierEvenement(int id);
}
