package services.interfaces;

import java.util.List;

import org.demo.bean.jpa.EvenementEntity;
import org.demo.bean.jpa.ParticipantEntity;
import org.demo.bean.jpa.UtilisateurEntity;

import exceptions.UtilisateurException;

public interface UtilisateurService {
	
	/**
	 * Creer un nouvel utilisateur en base de données.
	 * Permet l'inscription d'un utilisateur
	 * @param u Utilisateur inscrit
	 * @param p Participant associé à l'utilisateur 
	 * @throws UtilisateurException
	 */
	public void ajouter(UtilisateurEntity u, ParticipantEntity p)throws UtilisateurException;
	
	/**
	 * Reccupère la liste des evenements qu'un utilisateur a créé
	 * @param id de l'utilisateur
	 * @return liste des evenements
	 */
	public List<EvenementEntity> listeEvenementCree(Integer id);
	
	/**
	 * Effectue la vérification de connexion
	 * Permet d'authentifier un utilisateur
	 * @param mdp mot de passe de l'utilisateur
	 * @param email email de l'utilisateur
	 * @return id de l'utilisateur correctement identifé
	 * @throws UtilisateurException Si les données de connexion sont incorrectes
	 */
	public Integer checkUtilisateur(String mdp, String email) throws UtilisateurException;
	
	/**
	 * Retourne l'objet utilisateur dont l'id est donné en parametre
	 * @param id de l'utilisateur recherché
	 * @return Utilisateur trouvé
	 */
	public UtilisateurEntity getUtilisateur(Integer id);
}
