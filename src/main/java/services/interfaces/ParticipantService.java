package services.interfaces;

import java.util.List;

import org.demo.bean.jpa.EvenementEntity;
import org.demo.bean.jpa.ParticipantEntity;

import exceptions.ParticipantException;

public interface ParticipantService {
	
	/**
	 * Ajouter un nouveau participant 
	 * Vérifie que les données sont correctes
	 * 
	 * @param participant participant que l'on veut ajouter en base de données
	 * @throws ParticipantException si les informations du participant sont incorrectes
	 */
	public void ajouter(ParticipantEntity participant) throws ParticipantException;
	
	/**
	 * Inscrit un participant donné à un evenement
	 * 
	 * @param idPa
	 * @param idEv
	 * @throws ParticipantException
	 */
	public void inscrireEvenement(Integer idPa, Integer idEv)
			throws ParticipantException;

	/**
	 * Pour un participant, liste tous les evenements auquels il participe
	 * @param id id du participant 
	 * @return liste des evenements 
	 */
	public List<EvenementEntity> listeEvenements(Integer id);
	
	/**
	 * Retourne le participant possedant l'adresse email donnée
	 * @param email du participant
	 * @return Participant recherché
	 */
	public ParticipantEntity getParticipant(String email);
}
