package services;

import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.validation.Validation;
import javax.validation.Validator;

import org.demo.bean.jpa.EvenementEntity;
import org.demo.bean.jpa.ParticipantEntity;
import org.demo.bean.jpa.ParticipeEntity;
import org.demo.persistence.services.ParticipantPersistence;
import org.demo.persistence.services.jpa.EvenementPersistenceJPA;
import org.demo.persistence.services.jpa.ParticipantPersistenceJPA;
import org.hibernate.Hibernate;

import services.interfaces.ParticipantService;
import exceptions.ParticipantException;

public class ParticipantServiceImpl implements ParticipantService {

	@Override
	public void ajouter(ParticipantEntity participant)
			throws ParticipantException {
		Validator validator = Validation.buildDefaultValidatorFactory()
				.getValidator();
		
		if (validator.validate(participant).size() != 0) {
			throw new ParticipantException(
					ParticipantException.PARTICIPANT_INVALIDE);
		}
		ParticipantPersistence ppj = new ParticipantPersistenceJPA();
		ppj.insert(participant);

	}

	@Override
	public void inscrireEvenement(Integer idPa, Integer idEv)
			throws ParticipantException {
		EvenementPersistenceJPA epj = new EvenementPersistenceJPA();
		HashMap<String, Object> eventMap = new HashMap<String, Object>();
		eventMap.put("id", idEv);
		if (epj.search(eventMap).size() == 1) {
			EntityManagerFactory emf = Persistence
					.createEntityManagerFactory("persistence-unit1");
			EntityManager em = emf.createEntityManager();
			em.getTransaction().begin();

			ParticipeEntity participe = new ParticipeEntity();
			participe.setIdEv(idEv);
			participe.setIdUt(idPa);
			em.persist(participe);

			em.getTransaction().commit();
			{

			}
		} else {
			throw new ParticipantException(
					ParticipantException.EVENEMENT_INNEXISTANT);
		}

	}

	@Override
	public List<EvenementEntity> listeEvenements(Integer id) {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("persistence-unit1");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		ParticipantEntity participant = em.find(ParticipantEntity.class, id);
		List<EvenementEntity> participants = participant.getListOfEvenement();
		Hibernate.initialize(participants);
		em.close();
		return participants;
	}

	@Override
	public ParticipantEntity getParticipant(String email) {
		ParticipantPersistenceJPA pp = new ParticipantPersistenceJPA();
		HashMap<String, Object> participantMap = new HashMap<String, Object>();
		participantMap.put("email", email);
		if (pp.search(participantMap).size() == 1) {
			return pp.search(participantMap).get(0);

		} else
			return null;
	}

}
