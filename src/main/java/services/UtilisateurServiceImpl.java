package services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.demo.bean.jpa.EvenementEntity;
import org.demo.bean.jpa.ParticipantEntity;
import org.demo.bean.jpa.UtilisateurEntity;
import org.demo.persistence.services.UtilisateurPersistence;
import org.demo.persistence.services.jpa.EvenementPersistenceJPA;
import org.demo.persistence.services.jpa.UtilisateurPersistenceJPA;

import services.interfaces.UtilisateurService;
import exceptions.UtilisateurException;

public class UtilisateurServiceImpl implements UtilisateurService {

	@Override
	public void ajouter(UtilisateurEntity u, ParticipantEntity p)
			throws UtilisateurException {

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("persistence-unit1");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(p);
		em.getTransaction().commit();
		em.getTransaction().begin();
		em.persist(u);
		em.getTransaction().commit();
		em.close();

	}

	@Override
	public List<EvenementEntity> listeEvenementCree(Integer id) {
		EvenementPersistenceJPA epj = new EvenementPersistenceJPA();
		HashMap<String, Object> creerMap = new HashMap<String, Object>();
		creerMap.put("utilisateur", id);
		return epj.search(creerMap);
	}

	@Override
	public Integer checkUtilisateur(String mdp, String email)
			throws UtilisateurException {
		if (email == null || email.equals("")) {
			throw new UtilisateurException(UtilisateurException.UTILISATEUR_MAIL_INVALIDE);
		}
		if (mdp == null || mdp.equals("")) {
			throw new UtilisateurException(UtilisateurException.UTILISATEUR_MDP_INVALIDE);
		}
		
		UtilisateurPersistence utilisateurPersistance = new UtilisateurPersistenceJPA();
		
		Map<String, Object> criteria = new HashMap<>();
		criteria.put("email", email);
		List<UtilisateurEntity> utilisateurs = utilisateurPersistance.search(criteria);
		
		if(utilisateurs.isEmpty()){
			throw new UtilisateurException(UtilisateurException.UTILISATEUR_INNEXISTANT);
			
		}
		else if(mdp.equals(utilisateurs.get(0).getMdp())){
			return utilisateurs.get(0).getId();
		}
		else{
			throw new UtilisateurException(UtilisateurException.UTILISATEUR_INNEXISTANT);
		}
		
	}

	@Override
	public UtilisateurEntity getUtilisateur(Integer id) {
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("persistence-unit1");
		EntityManager em = emf.createEntityManager();
		return em.find(UtilisateurEntity.class, id);
	}

}
