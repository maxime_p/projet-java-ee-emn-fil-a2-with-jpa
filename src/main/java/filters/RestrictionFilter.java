package filters;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.demo.bean.jpa.EvenementEntity;

public class RestrictionFilter implements Filter {
	public static final String ACCES_connexion = "/connexion";
	public static final String ATT_SESSION_USER = "authentification";

	public void init(FilterConfig config) throws ServletException {
	}

	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		/* Cast des objets request et response */
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		/* R�cup�ration de la session depuis la requ�te */
		HttpSession session = request.getSession();

		/* Non-filtrage des ressources statiques */
		String chemin = request.getRequestURI().substring(
				request.getContextPath().length());
		if (chemin.startsWith("/inc")) {
			chain.doFilter(request, response);
			return;
		}

		if (chemin.startsWith("/connexion")
				|| chemin.startsWith("/inscription")) {
			chain.doFilter(request, response);
			return;
		}

		/**
		 * Si l'objet utilisateur n'existe pas dans la session en cours, alors
		 * l'utilisateur n'est pas connect�.
		 */
		if (session.getAttribute(ATT_SESSION_USER) == null) {
			if (session.getAttribute("msg_spy") != null) {
				if ((int) session.getAttribute("msg_spy") == 1) {
					session.setAttribute("msg_spy", 0);
				} else {
					session.setAttribute("message", null);
				}
			}
			String event = request.getParameter("e");
			EntityManagerFactory emf = Persistence
					.createEntityManagerFactory("persistence-unit1");
			EntityManager em = emf.createEntityManager();
			if (event != null && chemin.startsWith("/afficherinfo")) {
				EvenementEntity e = em.find(EvenementEntity.class,
						Integer.parseInt(event));
				if (e != null) {
					Boolean publie = (e.getPublie() == 1) ? true : false;
					if (publie) {
						chain.doFilter(request, response);
					} else {
						/* Redirection vers la page publique */
						request.getRequestDispatcher(ACCES_connexion).forward(
								request, response);
					}
				} else {
					/* Redirection vers la page publique */
					request.getRequestDispatcher(ACCES_connexion).forward(
							request, response);
				}
			} else {
				/* Redirection vers la page publique */
				request.getRequestDispatcher(ACCES_connexion).forward(request,
						response);
			}
		} else {
			/* Affichage de la page restreinte */
			if (session.getAttribute("msg_spy") != null) {
				if ((int) session.getAttribute("msg_spy") == 1) {
					session.setAttribute("msg_spy", 0);
				} else {
					session.setAttribute("message", null);
				}
			}
			chain.doFilter(request, response);
		}
	}

	public void destroy() {
	}
}