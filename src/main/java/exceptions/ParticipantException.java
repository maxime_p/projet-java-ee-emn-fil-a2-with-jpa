package exceptions;

public class ParticipantException extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = -5129579051216690204L;
	
	public static final String EVENEMENT_INNEXISTANT = "L'événement n'existe pas";

	public static final String PARTICIPANT_INVALIDE = "Les informations sont invalides";

	private String information;
	
	public ParticipantException(String information) {
		this.information = information;
	}
	
	public String getInformation(){
		return this.information;
	}

}
