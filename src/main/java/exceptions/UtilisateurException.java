package exceptions;

public class UtilisateurException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1230095140263812679L;

	private String information;
	
	public static final String UTILISATEUR_MDP_INVALIDE = "Le mot de passe doit etre renseigné";
	public static final String UTILISATEUR_MAIL_INVALIDE = "L'adresse email doit etre renseignée";
	public static final String UTILISATEUR_INNEXISTANT = "Identifant incorrect";

	public UtilisateurException(String information) {
		this.information = information;
	}

	public String getInformation() {
		return this.information;
	}

}
