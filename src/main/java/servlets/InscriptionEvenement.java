package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.demo.bean.jpa.EvenementEntity;
import org.demo.bean.jpa.ParticipantEntity;
import org.demo.bean.jpa.UtilisateurEntity;
import org.demo.persistence.services.EvenementPersistence;
import org.demo.persistence.services.jpa.EvenementPersistenceJPA;

import services.ParticipantServiceImpl;
import services.interfaces.ParticipantService;
import exceptions.ParticipantException;

/**
 * Servlet implementation class RegisterToEvent
 */
public class InscriptionEvenement extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InscriptionEvenement() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		System.out.println("get");

		HttpSession session = request.getSession();
		Integer eventID = Integer.parseInt(request.getParameter("e"));
		UtilisateurEntity user = (UtilisateurEntity) session
				.getAttribute("authentification");

		ParticipantService participantService = new ParticipantServiceImpl();

		try {
			participantService.inscrireEvenement(user.getParticipant().getId(),
					eventID);
			session.setAttribute("inscriptionOK", true);
			response.sendRedirect("./afficherinfo?e=" + eventID);
		} catch (ParticipantException e) {
			session.setAttribute("inscriptionKO", true);
			response.sendRedirect("./afficherinfo?e=" + eventID);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		System.out.println("post");

		// VIA POST : l'utilisateur est annonyme et utilise le formulaire
		// d'inscription
		HttpSession session = request.getSession();
		Integer eventID = Integer.parseInt(request.getParameter("e"));
		EvenementPersistence epj = new EvenementPersistenceJPA();
		EvenementEntity event = epj.load(eventID);

		ParticipantService partService = new ParticipantServiceImpl();
		ParticipantEntity participant = new ParticipantEntity();

		participant.setEmail(request.getParameter("inputEmail"));
		participant.setNom(request.getParameter("inputNom"));
		participant.setPrenom(request.getParameter("inputPrenom"));
		participant.setSociete(request.getParameter("inputSociete"));
		
		try {
			partService.ajouter(participant);
			partService.inscrireEvenement(participant.getId(), event.getId());
			session.setAttribute("inscriptionOK", true);
			response.sendRedirect("./afficherinfo?e=" + event.getId());
			
		} catch (ParticipantException e) {
			System.out.println(e.getInformation());
			session.setAttribute("inscriptionKO", true);
			response.sendRedirect("./afficherinfo?e=" + event.getId());
		}
	}

}
