package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.demo.bean.jpa.UtilisateurEntity;

import services.ParticipantServiceImpl;
import services.UtilisateurServiceImpl;
import services.interfaces.ParticipantService;
import services.interfaces.UtilisateurService;

/**
 * Servlet implementation class Index
 */
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String TITRE = "Event EE - Index";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Index() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		UtilisateurEntity user = (UtilisateurEntity) session
				.getAttribute("authentification");
		
		Integer idParticipant = user.getParticipant().getId();
		UtilisateurService utilisateurService = new UtilisateurServiceImpl();
		ParticipantService participantService = new ParticipantServiceImpl();
		
		//Envoi des deux listes d'evenements dans la jsp
		request.setAttribute("evenements", utilisateurService.listeEvenementCree(user.getId()));
		request.setAttribute("participe", participantService.listeEvenements(idParticipant));
		
		RequestDispatcher rd;
		rd = request.getRequestDispatcher("/WEB-INF/jsp/accueil.jsp");
		request.setAttribute("titre", TITRE);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd;
		rd = request.getRequestDispatcher("/WEB-INF/jsp/accueil.jsp");
		rd.forward(request, response);
	}

}
