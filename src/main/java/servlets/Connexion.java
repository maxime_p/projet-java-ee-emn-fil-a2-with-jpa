package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.demo.bean.jpa.UtilisateurEntity;

import exceptions.UtilisateurException;
import services.UtilisateurServiceImpl;
import services.interfaces.UtilisateurService;

/**
 * Servlet implementation class Login
 */
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String TITRE = "Event EE - Connexion";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Connexion() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd;
		rd = request.getRequestDispatcher("WEB-INF/jsp/connexion.jsp");
		request.setAttribute("titre", TITRE);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	private void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String email = request.getParameter("inputEmail");
		String motDePasse = request.getParameter("inputPassword");
		HttpSession session = request.getSession();
		session.setAttribute("message", null);
		session.setAttribute("msg_spy", 0);

		try {
			UtilisateurService userService = new UtilisateurServiceImpl();
			Integer id = userService.checkUtilisateur(motDePasse, email);

			UtilisateurEntity user = userService.getUtilisateur(id);
			// --- Stocker un utilisateur en session :
			session.setAttribute("authentification", user);
			session.setAttribute("message", null);
			session.setAttribute("msg_spy", 0);

			response.sendRedirect("");

		} catch (UtilisateurException e) {

			session.setAttribute("authentification", null);
			session.setAttribute("message", e.getInformation());
			session.setAttribute("msg_spy", 1);

			response.sendRedirect("");
		}

	}

}
