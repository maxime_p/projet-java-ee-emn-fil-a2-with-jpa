package servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.demo.bean.jpa.ParticipantEntity;
import org.demo.bean.jpa.UtilisateurEntity;
import org.demo.persistence.services.jpa.ParticipantPersistenceJPA;

import exceptions.UtilisateurException;
import services.ParticipantServiceImpl;
import services.UtilisateurServiceImpl;
import services.interfaces.ParticipantService;
import services.interfaces.UtilisateurService;

/**
 * Servlet implementation class Inscription
 */
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String TITRE = "Event EE - Inscription";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Inscription() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher rd;
		rd = request.getRequestDispatcher("WEB-INF/jsp/inscription.jsp");
		HttpSession session = request.getSession(false);
		session.setAttribute("message", null);
		session.setAttribute("msg_spy", 0);
		request.setAttribute("titre", TITRE);
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher rd;
		rd = request.getRequestDispatcher("WEB-INF/jsp/inscription.jsp");
		HttpSession session = request.getSession(false);

		String email = request.getParameter("inputEmail");
		String nom = request.getParameter("inputNom");
		String prenom = request.getParameter("inputPrenom");
		String societe = request.getParameter("inputSociete");
		String motDePasse = request.getParameter("inputPassword");
		String motDePasse2 = request.getParameter("inputPassword2");

		if (email != "" && motDePasse != "" && motDePasse2 != "" && nom != ""
				&& prenom != "") {
			if (!motDePasse.equals(motDePasse2)) {
				session.setAttribute("message",
						"Les deux mots de passe doivent correspondre.");
				session.setAttribute("msg_spy", 1);

			} else {
				if (motDePasse.length() < 8) {
					session.setAttribute("message",
							"Le mot de passe doit contenir au moins 8 caract�res.");
					session.setAttribute("msg_spy", 1);
				} else {
					ParticipantEntity p = new ParticipantEntity();
					p.setEmail(email);
					p.setNom(nom);
					p.setPrenom(prenom);
					p.setSociete(societe);
					ParticipantService partService = new ParticipantServiceImpl();
					p = partService.getParticipant(p.getEmail());
					UtilisateurEntity user = new UtilisateurEntity();
					user.setEmail(email);
					user.setMdp(motDePasse);
					user.setParticipant(p);
					UtilisateurService userService = new UtilisateurServiceImpl();
					try {
						userService.ajouter(user, p);
					} catch (UtilisateurException e) {
						e.printStackTrace();
					}
					session.setAttribute("message", null);
					session.setAttribute("msg_spy", 0);
					rd = request
							.getRequestDispatcher("WEB-INF/jsp/connexion.jsp");
				}
			}
		} else {
			session.setAttribute("message", "Veuillez remplir tous les champs.");
			session.setAttribute("msg_spy", 1);
		}
		System.out.println(session.getAttribute("message"));
		rd.forward(request, response);
	}
}
