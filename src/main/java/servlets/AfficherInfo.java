package servlets;

import java.io.IOException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.demo.bean.jpa.EvenementEntity;
import org.demo.bean.jpa.ParticipantEntity;
import org.demo.bean.jpa.UtilisateurEntity;
import org.hibernate.Hibernate;

/**
 * Servlet implementation class afficherinfo
 */
public class AfficherInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String TITRE = "Event EE - Evenement";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AfficherInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd;

		// Reccuperer l'ID de l'événément choisi
		HttpSession session = request.getSession();
		try {
			Integer event = Integer.parseInt(request.getParameter("e"));
			UtilisateurEntity user = (UtilisateurEntity) session
					.getAttribute("authentification");
			EntityManagerFactory emf = Persistence
					.createEntityManagerFactory("persistence-unit1");
			EntityManager em = emf.createEntityManager();

			em.getTransaction().begin();
			EvenementEntity e = em.find(EvenementEntity.class, event);
			if (e == null) {
				request.getRequestDispatcher("/WEB-INF/jsp/404.jsp").forward(
						request, response);
			}

			List<ParticipantEntity> parts = e.getListOfParticipant();
			Hibernate.initialize(parts);
			em.close();

			if (session.getAttribute("inscriptionOK") != null
					&& (boolean) session.getAttribute("inscriptionOK")) {
				request.setAttribute("inscriptionOK", true);
				session.setAttribute("inscriptionOK", false);
			}
			if (session.getAttribute("inscriptionKO") != null
					&& (boolean) session.getAttribute("inscriptionKO")) {
				request.setAttribute("inscriptionKO", true);
				session.setAttribute("inscriptionKO", false);
			}

			request.setAttribute("titre", TITRE);
			request.setAttribute("participe", parts);
			request.setAttribute("evenement", e);
			request.setAttribute("annonyme", user == null);
			if (user != null) {
				request.setAttribute("auteur",
						user.getId().equals(e.getUtilisateur().getId()));
				Boolean inscription = false;
				for (ParticipantEntity p : parts) {
					if (user.getParticipant().getId().equals(p.getId())) {
						inscription = true;
					}
				}
				request.setAttribute("inscription", inscription);
			}

			// evenement n'existe pas ou n est pas publie
			// request.getRequestDispatcher("./").forward(request, response);
			rd = request.getRequestDispatcher("/WEB-INF/jsp/details.jsp");
			rd.forward(request, response);

		} catch (NumberFormatException e) {
			request.getRequestDispatcher("/WEB-INF/jsp/404.jsp").forward(
					request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
