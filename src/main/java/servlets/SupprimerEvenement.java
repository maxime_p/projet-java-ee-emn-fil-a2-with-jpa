package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.demo.bean.jpa.UtilisateurEntity;

import services.EvenementServiceImpl;
import services.interfaces.EvenementService;

/**
 * Servlet implementation class supprimerevenement
 */
public class SupprimerEvenement extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SupprimerEvenement() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        HttpSession session = request.getSession();

        Integer event = Integer.parseInt(request.getParameter("e"));
        UtilisateurEntity user = (UtilisateurEntity) session
                .getAttribute("authentification");

        EvenementService serviceE = new EvenementServiceImpl();
        serviceE.supprimerEvenement(event);

        response.sendRedirect("./");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

}
