package servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.demo.bean.jpa.EvenementEntity;
import org.demo.bean.jpa.UtilisateurEntity;

import services.EvenementServiceImpl;
import services.interfaces.EvenementService;
import exceptions.EvenementException;

/**
 * Servlet implementation class CreateEvent
 */
public class CreerEvenement extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static final String TITRE = "Event EE - Creer evenement";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreerEvenement() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("WEB-INF/jsp/ajouter.jsp");
        request.setAttribute("titre", TITRE);
        rd.forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        process(request, response);

    }

    private void process(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        String nom = request.getParameter("nomEvent");
        String adresse = request.getParameter("adresseEvent");
        String dateDebut = request.getParameter("dateDebutEvent");
        String dateFin = request.getParameter("dateFinEvent");
        String heureDebut = request.getParameter("heureDebutEvent");
        String heureFin = request.getParameter("heureFinEvent");
        int publie = 0;
        if (request.getParameter("publie") != null) {
            publie = request.getParameter("publie").equals("on") ? 1 : 0;
        }
        if (nom == null) {
            nom = "";
        }
        if (adresse == null) {
            nom = "";
        }
        if (dateDebut == null) {
            nom = "";
        }
        if (dateFin == null) {
            nom = "";
        }
        if (heureDebut == null) {
            nom = "";
        }
        if (heureFin == null) {
            nom = "";
        }

        if (nom != "" && adresse != "" && dateDebut != "" && dateFin != ""
                && heureDebut != "" && heureFin != "") {

            UtilisateurEntity user = (UtilisateurEntity) session
                    .getAttribute("authentification");
            EvenementEntity event = new EvenementEntity();
            event.setNom(nom);
            event.setAdresse(adresse);
            SimpleDateFormat dFormatter = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat hformatter = new SimpleDateFormat("hh:mm");
            Date dDebut = null;
            Date dFin = null;
            Date hDebut = null;
            Date hFin = null;
            try {
                dDebut = dFormatter.parse(dateDebut);
                dFin = dFormatter.parse(dateFin);
                hDebut = hformatter.parse(heureDebut);
                hFin = hformatter.parse(heureFin);
                event.setDebut(dDebut);
                event.setFin(dFin);
                event.setHdebut(hDebut);
                event.setHfin(hFin);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            event.setUtilisateur(user);
            event.setPublie(publie);
            // Cr�ation dans la base de donn�es

            EvenementService serviceE = new EvenementServiceImpl();
            try {
                serviceE.creerEvenement(event);
            } catch (EvenementException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            session.setAttribute("message", "Ajout de l'evenement " + nom
                    + " avec succes !");
            session.setAttribute("msg_spy", 1);
            response.sendRedirect("./creerevenement");

        } else {
            session.setAttribute("message",
                    "Erreur(s) dans l'ajout de l'evenement (parametre(s) manquant(s)) !");
            session.setAttribute("msg_spy", 1);
            response.sendRedirect("./creerevenement");
        }

    }

}
